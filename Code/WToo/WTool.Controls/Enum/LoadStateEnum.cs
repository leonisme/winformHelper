﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WTool.Controls.Enum
{
    /// <summary>
    /// 加载中弹窗的样式
    /// </summary>
    public enum LoadStateEnum
    {
        LoadStateBlue,
        LoadStateRed
    }
}
