﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WTool.Controls.Enum
{
    /// <summary>
    /// 鼠标加载类型
    /// </summary>
    public enum MouseActionType
    {
        None,//无
        Hover,//鼠标移入上方并释放
        Click//鼠标点击
    }
}
