﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WTool.Controls.Model
{
    public class LoadDialog
    {
        Size size = new Size(0,0);

        Image image;
        /// <summary>
        /// 样式的大小
        /// </summary>
        public Size Size { get => size; set => size = value; }

        /// <summary>
        /// 样式的图片
        /// </summary>
        public Image Image { get => image; set => image = value; }
    }
}
