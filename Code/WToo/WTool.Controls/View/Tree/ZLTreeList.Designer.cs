﻿namespace WTool.Controls.View.Tree
{
    partial class ZLTreeList
    {
        /// <summary> 
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 组件设计器生成的代码

        /// <summary> 
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.ZDPan = new WTool.Controls.View.Panels.ZLDoublePanel(this.components);
            this.SuspendLayout();
            // 
            // ZDPan
            // 
            this.ZDPan.AutoScroll = true;
            this.ZDPan.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ZDPan.Location = new System.Drawing.Point(0, 0);
            this.ZDPan.Name = "ZDPan";
            this.ZDPan.Size = new System.Drawing.Size(300, 145);
            this.ZDPan.TabIndex = 0;
            // 
            // ZLTreeList
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.ZDPan);
            this.Name = "ZLTreeList";
            this.Size = new System.Drawing.Size(300, 145);
            this.Load += new System.EventHandler(this.ZLTreeList_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private Panels.ZLDoublePanel ZDPan;
    }
}
