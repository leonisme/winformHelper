﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing.Drawing2D;

namespace WTool.Controls.View.Tree
{
    public partial class ZLTreeList : UserControl
    {
        public ZLTreeList()
        {
            InitializeComponent();
        }

        internal TreeNode root= new TreeNode();

        internal TreeNodeCollection nodes;

        [Category("CatBehavior")]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
        [Localizable(true)]
        [Description("节点集合")]
        [MergableProperty(false)]
        public TreeNodeCollection Nodes
        {
            get
            {
                if (nodes == null)
                {
                    nodes = root.Nodes;
                }

                return nodes;
            }
        }



        /// <summary>
        /// 获取点击的节点
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ZLTreeList_Load(object sender, EventArgs e)
        {
            SetControl(nodes);
        }


        Point pi = new Point(10, 12);
        private void ClearControl()
        {
            ZDPan.Controls.Clear();
            pi = new Point(10, 12);
        }
        /// <summary>
        /// 绘制菜单
        /// </summary>
        private void SetControl(TreeNodeCollection nodeItem, string iex="",bool mother=true,int cou=0)
        {
            if (nodes != null&& nodes.Count>0)
            {
                int count = cou;
                foreach (TreeNode tn in nodeItem)
                {
                    Label lab = new Label();
                    lab.Tag = iex+tn.Index;
                    lab.AutoSize = false;
                    lab.TextAlign = ContentAlignment.MiddleLeft;
                    /* 绘制父节点
                     * **/
                    if (mother)
                    {
                        lab.Click += motherLab_Click;
                        lab.Text = "          " + tn.Text;
                        lab.Size = new Size(280, 41);
                        lab.Location = new Point(pi.X, pi.Y);
                        lab.Font = new Font("Microsoft YaHei", 14, FontStyle.Bold);
                        if (tn.Checked)
                        {
                            lab.ForeColor = Color.FromArgb(255, 255, 255);
                            lab.Image = Properties.Resources.OneNode;
                        }
                        else
                        {
                            lab.ForeColor = Color.FromArgb(51, 51, 51);
                            lab.Image = Properties.Resources.OneNodeFalse;
                        }
                        pi.Y += lab.Height + 16;
                    }
                    /* 绘制子节点
                     * 判断是否为最后一层
                     * 为最后一层绘制最后一层子节点
                     * 不为最后一层绘制非最后一层子节点
                     * **/
                    else
                    {
                        /* 绘制最后一层子节点
                         * **/
                        if (tn.Nodes.Count == 0)
                        {
                            
                            lab.Text = "        " + tn.Text;
                            lab.Size = new Size(256, 28);
                            lab.Location = new Point(pi.X + count * 15, pi.Y);
                            lab.Font = new Font("Microsoft YaHei", 12, FontStyle.Regular);
                            lab.ForeColor = Color.FromArgb(102, 102, 102);
                            lab.MouseMove += lab_MouseMove;
                            lab.MouseLeave += lab_MouseLeave;
                            lab.Click += lab_Click;
                            lab.Image = Properties.Resources.threefalse;
                            pi.Y += lab.Height + 13;
                        }
                        /* 绘制非最后一层子节点
                         * **/
                        else
                        {
                            lab.Text ="     " + tn.Text;
                            lab.Size = new Size(300,28);
                            lab.Location = new Point(pi.X + count * 15, pi.Y);
                            lab.Font = new Font("Microsoft YaHei", 14, FontStyle.Bold);
                            lab.ForeColor = Color.FromArgb(51, 51, 51);
                            lab.Click += motherLab_Click;
                            if (tn.Checked)
                            {
                                lab.Image = Properties.Resources.TwoNode;
                            }
                            else
                            {
                                lab.Image = Properties.Resources.TwoNodeFalse;
                            }
                            pi.Y += lab.Height + 13;
                        }
                    }
                    ZDPan.Controls.Add(lab);
                    
                    /* 判断是否是选中
                     * 选中：是否存在子节点
                     * 存在子节点：绘制子节点
                     * 绘制完子节点后递归层数减1并且绘制横线
                     * **/
                    if (tn.Checked)
                    {
                        if (tn.Nodes.Count > 0)
                        {
                            SetControl(tn.Nodes, iex + tn.Index + ",", false, count += 1);
                            count--;
                            if (!mother && tn.Nodes.Count != 0)
                            {
                                Label line = new Label();
                                line.AutoSize = false;
                                line.Size = new Size(250, 1);
                                line.Location = new Point(pi.X + count * 10 + 12, pi.Y - 6);
                                line.Font = new Font("Microsoft YaHei", 14, FontStyle.Bold);
                                line.Image = Properties.Resources.Line;
                                line.BringToFront();
                                ZDPan.Controls.Add(line);
                            }
                        }
                    }
                }
            }
        }

        /// <summary>
        /// 鼠标点击效果
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void motherLab_Click(object sender, EventArgs e)
        {
            Label lab = (Label)sender;
            string[] c = lab.Tag.ToString().Split(',');
            SetClick(Nodes, c);
            ClearControl();
            SetControl(nodes);
        }

        /// <summary>
        /// 改变点击的索引的值
        /// </summary>
        /// <param name="index"></param>
        private bool SetClick(TreeNodeCollection tnItem,string[] indexItem,int count=0)
        {
            foreach (TreeNode tn in tnItem)
            {
                if (tn.Index == int.Parse(indexItem[count]))
                {
                    if (count == indexItem.Length-1)
                    {//选中的值
                        tn.Checked = tn.Checked ? false : true;
                        return true;
                    }
                    else
                    {
                        count++;
                        if (SetClick(tn.Nodes, indexItem, count))
                        {
                            break;
                        }
                    }
                }
            }
            return false;
        }

        /// <summary>
        /// 最后一次点击的节点
        /// </summary>
        public TreeNode SelectNode;//最后一次点击时的节点
        /// <summary>
        /// 改变点击的索引的值
        /// </summary>
        /// <param name="index"></param>
        private bool GetClick(TreeNodeCollection tnItem, string[] indexItem, int count = 0)
        {
            bool select = false;
            foreach (TreeNode tn in tnItem)
            {
                if (tn.Index == int.Parse(indexItem[count]))
                {
                    if (count == indexItem.Length - 1)
                    {//选中的值
                        SelectNode = tn;
                        select = true;
                    }
                    else
                    {
                        count++;
                        select = GetClick(tn.Nodes, indexItem, count);
                        if (select)
                        {
                            break;
                        }
                    }
                }
            }
            return select;
        }

        /// <summary>
        /// 最后一层鼠标移入效果
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void lab_MouseMove(object sender, MouseEventArgs e)
        {
            Label lab = (Label)sender;
            lab.Image = Properties.Resources.three;
        }

        /// <summary>
        /// 最后一层鼠标移出效果
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void lab_MouseLeave(object sender, EventArgs e)
        {
            Label lab = (Label)sender;
            lab.Image = Properties.Resources.threefalse;
        }

        
        public delegate void MyDelegate(object sender, EventArgs e);
        [Description("最后一层点击事件")]
        public event MyDelegate ClickEvent;

        /// <summary>
        /// 最后一层鼠标点击效果
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void lab_Click(object sender, EventArgs e)
        {
            Label lab = (Label)sender;
            string[] c = lab.Tag.ToString().Split(',');
            GetClick(Nodes, c);
            ClickEvent?.Invoke(sender, e);
        }
    }
}
