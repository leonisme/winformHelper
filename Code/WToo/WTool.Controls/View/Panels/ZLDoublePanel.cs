﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WTool.Controls.View.Panels
{
    /// <summary>
    /// 双缓冲panel
    /// </summary>
    public partial class ZLDoublePanel : Panel
    {
        public ZLDoublePanel()
        {
            InitializeComponent();
            this.DoubleBuffered = true;//设置本窗体
            SetStyle(ControlStyles.UserPaint| ControlStyles.AllPaintingInWmPaint| ControlStyles.DoubleBuffer, true);
        }

        public ZLDoublePanel(IContainer container)
        {
            container.Add(this);

            InitializeComponent();
            this.DoubleBuffered = true;//设置本窗体
            SetStyle(ControlStyles.UserPaint | ControlStyles.AllPaintingInWmPaint | ControlStyles.DoubleBuffer, true);
        }
    }
}
