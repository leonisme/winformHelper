﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WTool.Controls.Controls.ColorControls;

namespace WTool.Controls.View.Panels
{
    public class ZLPanel : Panel
    {

        /// <summary>
        /// 获取或设置边框的颜色
        /// </summary>
        private Color borderColor = LineColors.SkyBlue;
        [Description("边框颜色"), Category("自定义")]
        public Color BorderColor
        {
            get { return borderColor; }
            set
            {
                borderColor = value;
                this.Invalidate();
            }
        }


        /// <summary>
        /// 获取或设置左侧的颜色
        /// </summary>
        private Color leftColor = StatusColors.FaintYellow;
        [Description("左侧半圆颜色"), Category("自定义")]
        public Color LeftColor
        {
            get { return leftColor; }
            set
            {
                leftColor = value;
                this.Invalidate();
            }
        }

        /// <summary>
        /// 对象新实例的初始化
        /// </summary>
        public ZLPanel()
            : base()
        {//设置边距大小
            Padding = new Padding(4, 1, 1, 1);
        }

        /// <summary>
        /// 绘制
        /// </summary>
        /// <param name="e"></param>
        protected override void OnPaint(PaintEventArgs e)
        {
            base.OnPaint(e);
            e.Graphics.SmoothingMode = SmoothingMode.AntiAlias;  //使绘图质量最高，即消除锯齿
            e.Graphics.InterpolationMode = InterpolationMode.HighQualityBicubic;
            e.Graphics.CompositingQuality = CompositingQuality.HighQuality;

            //绘制边线
            e.Graphics.DrawLines(new Pen(borderColor), new Point[]
            {
                new Point(e.ClipRectangle.Left,e.ClipRectangle.Top),
                new Point(e.ClipRectangle.Right-1,e.ClipRectangle.Top),
                new Point(e.ClipRectangle.Right-1,e.ClipRectangle.Bottom-1),
                new Point(e.ClipRectangle.Left,e.ClipRectangle.Bottom-1),
                new Point(e.ClipRectangle.Left,e.ClipRectangle.Top)
            });
            //绘制左侧半圆
            e.Graphics.FillEllipse(new SolidBrush(leftColor), new Rectangle(-10, 0, 20, this.Height));
        }
    }
}
