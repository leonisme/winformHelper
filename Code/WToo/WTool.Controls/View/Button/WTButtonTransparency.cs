﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WTool.Controls.View.Button
{
    /// <summary>
    /// 透明按钮
    /// </summary>
    public partial class WTButtonTransparency : System.Windows.Forms.Button
    {
        public WTButtonTransparency()
        {
            InitializeComponent();
            this.BackColor = Color.Transparent;//设置背景透明
        }
    }
}
