﻿namespace WTool.Controls.View.Dialog
{
    partial class LoadDialog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labImage = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // labImage
            // 
            this.labImage.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labImage.Image = global::WTool.Controls.Properties.Resources.load_blue;
            this.labImage.Location = new System.Drawing.Point(0, 0);
            this.labImage.Name = "labImage";
            this.labImage.Size = new System.Drawing.Size(178, 88);
            this.labImage.TabIndex = 0;
            // 
            // LoadDialog
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(178, 88);
            this.Controls.Add(this.labImage);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "LoadDialog";
            this.Text = "LoadDialog";
            this.Load += new System.EventHandler(this.LoadDialog_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label labImage;
    }
}