﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WTool.Controls.Enum;

namespace WTool.Controls.View.Dialog
{
    public partial class LoadDialog : Form
    {
        public LoadDialog()
        {
            InitializeComponent();
        }
        #region 全局变量
        //加载框的样式
        LoadStateEnum loadState = LoadStateEnum.LoadStateRed;
        // 窗体的坐标
        Point locPoint = new Point(100, 100);
        /// <summary>
        /// 窗体的坐标
        /// </summary>
        public Point LocPoint { get => locPoint; set => locPoint = value; }
        /// <summary>
        /// 加载框的样式
        /// </summary>
        public LoadStateEnum LoadState { get => loadState; set => loadState = value; }
        #endregion

        #region 初始化
        /// <summary>
        /// 初始化
        /// 直接利用反射去加载类，可以避免再次判断，配合策略模式使用感觉很方便，同时需要添加也只需要在枚举里面加上需要加入的样式，可扩展性较强
        /// 获取到样式信息之后将样式信息设置到窗体
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void LoadDialog_Load(object sender, EventArgs e)
        {
            Assembly assembly = Assembly.GetExecutingAssembly(); // 获取当前程序集 
            dynamic obj = assembly.CreateInstance("WTool.Controls.View.Dialog." + LoadState.ToString());
            
            this.Location = LocPoint;
            Strategy strategy = new Strategy(obj);
            Model.LoadDialog model = strategy.GetStyle();
            this.Size = model.Size;
            labImage.Image = model.Image;
        }
        #endregion

        #region 打开
        private void ShowWaitForm()
        {
            this.BringToFront();//放在前端显示
            this.Activate(); //当前窗体是LoadingForm
            this.ShowDialog();
        }

        /// <summary>
        /// 显示加载窗体
        /// </summary>
        /// <param name="msg">显示文字</param>
        public void ShowLoading()
        {
            MethodInvoker invoker = new MethodInvoker(ShowWaitForm);
            invoker.BeginInvoke(null, null);
            /*Console.WriteLine("等待Loading窗体实例化"); */
            while (!this.IsHandleCreated) { }
            // 把显示窗体放到最前面
            this.Invoke(new MethodInvoker(() =>
            {
                this.BringToFront();//放在前端显示
                this.Activate(); //当前窗体是LoadingForm
            }));
        }

        #endregion

        #region 关闭
        /// <summary>
        /// 关闭loading窗体
        /// </summary>
        public void CloseLoading()
        {
            try
            {
                if (!this.IsDisposed)
                {
                    this.BeginInvoke(new Action(() =>
                    {
                        this.Close();
                    }));
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        } 
        #endregion
    }

    #region 策略模式获取样式信息
    /// <summary>
    /// 策略
    /// </summary>
    public class Strategy
    {
        ILoadState loadState;
        public Strategy(ILoadState load)
        {
            loadState = load;
        }

        public Model.LoadDialog GetStyle()
        {
            return loadState.GetImage();
        }
    }

    /// <summary>
    /// 样式接口
    /// </summary>
    public interface ILoadState
    {
        Model.LoadDialog GetImage();
    }

    /// <summary>
    /// 蓝色样式
    /// </summary>
    public class LoadStateBlue : ILoadState
    {
        public Model.LoadDialog GetImage()
        {
            Model.LoadDialog model = new Model.LoadDialog();
            model.Size = new Size(178, 88);
            model.Image = Properties.Resources.load_blue;
            return model;
        }
    }

    /// <summary>
    /// 红色样式
    /// </summary>
    public class LoadStateRed : ILoadState
    {
        public Model.LoadDialog GetImage()
        {
            Model.LoadDialog model = new Model.LoadDialog();
            model.Size = new Size(400, 400);
            model.Image = Properties.Resources.load_red;
            return model;
        }
    } 
    #endregion
}
