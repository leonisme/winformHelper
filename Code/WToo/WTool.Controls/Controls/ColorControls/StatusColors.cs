﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WTool.Controls.Controls.ColorControls
{
    /// <summary>
    /// 状态颜色
    /// </summary>
    public class StatusColors
    {
        private static Color faintYellow = ColorTranslator.FromHtml("#FFFFCC");//淡黄色
        private static Color skyBlue = ColorTranslator.FromHtml("#CCFFFF");//天空蓝
        private static Color pompadour = ColorTranslator.FromHtml("#FFCCCC");//淡粉色

        /// <summary>
        /// 淡黄色
        /// </summary>
        public static Color FaintYellow { get => faintYellow; set => faintYellow = value; }

        /// <summary>
        /// 天空蓝
        /// </summary>
        public static Color SkyBlue { get => skyBlue; set => skyBlue = value; }
        /// <summary>
        /// 淡粉色
        /// </summary>
        public static Color Pompadour { get => pompadour; set => pompadour = value; }
    }
}
