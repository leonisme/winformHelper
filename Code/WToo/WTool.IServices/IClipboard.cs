﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WTool.IServices
{
    /// <summary>
    /// 粘贴板服务类
    /// </summary>
    public interface IClipboard
    {
        /// <summary>
        /// 添加粘贴板数据
        /// </summary>
        /// <param name="content"></param>
        /// <returns></returns>
        bool InsertClipboard(string content);

        /// <summary>
        /// 查询指定条数粘贴板中信息
        /// </summary>
        /// <param name="count"></param>
        /// <returns></returns>
        DataTable CreateCent(string count);

        /// <summary>
        /// 清除粘贴板所有信息
        /// </summary>
        /// <returns></returns>
        int ClearData();
    }
}
