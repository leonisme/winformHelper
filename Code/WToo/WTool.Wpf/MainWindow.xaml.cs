﻿using Panuon.UI.Silver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using WTool.WPF.Models;
using WTool.WPF.ViewModels;

namespace WTool.WPF
{
    /// <summary>
    /// MainWindow.xaml 的交互逻辑
    /// </summary>
    public partial class MainWindow : WindowX, IComponentConnector
    {
        private static IDictionary<string, Type> _partialViewDic;//定义一个字典，用来存所有的控件
        public MainWindowViewModel ViewModel { get; set; }

        /// <summary>
        /// 通过反射将所有控件先录入词典
        /// </summary>
        static MainWindow()
        {
            _partialViewDic = new Dictionary<string, Type>();
            var assembly = AppDomain.CurrentDomain.GetAssemblies().FirstOrDefault(x => x.FullName.StartsWith("WTool.WPF"));
            assembly.GetTypes().Where(x =>
            x.Namespace.StartsWith("WTool.WPF.Views") &&
            x.IsSubclassOf(typeof(UserControl))).ToList()
            .ForEach(x =>
            _partialViewDic.Add(x.Name.Remove(x.Name.Length - 4), x));

        }

        public MainWindow()
        {
            InitializeComponent();
            ViewModel = new MainWindowViewModel();
            DataContext = ViewModel;
        }

        /// <summary>
        /// 集合的tag匹配词典的键来找出对应需要展示的子页
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void TvMenu_SelectedItemChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
        {
            if (!IsLoaded)//是否已经加载
                return;

            var selectedItem = TvMenu.SelectedItem as TreeViewItemModel;//当前选择项
            var tag = selectedItem.Tag;//获取选择的Tag
            if (tag.IsNullOrEmpty())
                return;

            if (_partialViewDic.ContainsKey(tag))//如果包含该键就可以证明存在该子页，存在则展示
                ContentControl.Content = Activator.CreateInstance(_partialViewDic[tag]);
            else
                ContentControl.Content = null;
        }
    }
}
