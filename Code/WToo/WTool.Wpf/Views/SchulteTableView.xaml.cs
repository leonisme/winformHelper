﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WTool.WPF.Views
{
    /// <summary>
    /// SchulteTableView.xaml 的交互逻辑
    /// </summary>
    public partial class SchulteTableView : UserControl
    {
        public SchulteTableView()
        {
            InitializeComponent();
        }
        #region 字段
        List<int> map = new List<int>();//随机数列表
        int lastSum = 0;//上次点击数 
        int maxSum = 0;//最大数
        System.Windows.Threading.DispatcherTimer timer = new System.Windows.Threading.DispatcherTimer();//ui定时器
        int time = 0;
        #endregion

        #region 方法
        /// <summary>
        /// 启动timer
        /// </summary>
        private void StartTimer()
        {
            timer.Tick += timer_Tick;
            timer.Interval = new TimeSpan(0,0,1);
            timer.Start();
        }

        private void InitMap()
        {
            if (com_difficulty.SelectedItem != null)
            {
                grid_content.Children.Clear();
                grid_content.RowDefinitions.Clear();
                grid_content.ColumnDefinitions.Clear();
                int selectValue = int.Parse(com_difficulty.SelectedItem.ToString().Substring(com_difficulty.SelectedItem.ToString().Length - 1, 1));
                //生成随机数列表
                Random random = new Random();
                int ran = random.Next(1, 100);
                List<int> cacheMap = new List<int>();
                for (int mapIndex = 0; mapIndex < selectValue * selectValue; mapIndex++)
                {
                    cacheMap.Add(mapIndex + ran);
                }
                lastSum = cacheMap[0] - 1;
                maxSum = cacheMap[cacheMap.Count-1];
                map = ListRegroup(cacheMap);
                //生成指定方格
                for (int i = 0; i < selectValue; i++)
                {
                    grid_content.RowDefinitions.Add(new RowDefinition());
                    grid_content.ColumnDefinitions.Add(new ColumnDefinition());
                }
                //生成按钮
                for (int row = 0; row < selectValue; row++)
                {
                    for (int col = 0; col < selectValue; col++)
                    {
                        Button btn = new Button();
                        btn.Width = (grid_content.ActualWidth / selectValue) - 3;
                        btn.Height = grid_content.ActualHeight / selectValue - 3;
                        btn.Content = map[row * selectValue + col];
                        btn.Click += btn_Click;
                        Grid.SetRow(btn, row);
                        Grid.SetColumn(btn, col);
                        grid_content.Children.Add(btn);
                    }
                }
                StartTimer();
            }
        }

        /// <summary>
        /// 打乱list顺序
        /// </summary>
        private List<int> ListRegroup(List<int> myList)
        {
            Random ran = new Random();
            int index = 0;
            int temp = 0;
            for (int i = 0; i < myList.Count; i++)
            {
                index = ran.Next(0, myList.Count - 1);
                if (index != i)
                {
                    temp = myList[i];
                    myList[i] = myList[index];
                    myList[index] = temp;
                }
            }
            return myList;
        }
        #endregion

        #region 事件
        /// <summary>
        /// 定时器事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void timer_Tick(object sender, EventArgs e)
        {
            time++;
            lab_time.Content = "时间：" + time;
        }

        private void com_difficulty_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            InitMap();
        }

        private void btn_Click(object sender, RoutedEventArgs e)
        {
            Button btn = sender as Button;
            if (lastSum + 1 == int.Parse(btn.Content.ToString()))
            {
                btn.Background = Brushes.Green;
                lastSum++;
                if (lastSum == maxSum)
                {
                    MessageBox.Show("成功，重新开始");
                }
            }
            else
            {
                MessageBox.Show("失败，重新开始");
                InitMap();
            }
        }
        #endregion

    }
}
