﻿using Panuon.UI.Silver.Core;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WTool.WPF.Models;

namespace WTool.WPF.ViewModels
{
    public class MainWindowViewModel : PropertyChangedBase
    {
        public MainWindowViewModel()
        {
            MenuItems = new ObservableCollection<TreeViewItemModel>()
            {
                new TreeViewItemModel("windows工具","Windows", "\uf17a")
                {
                    MenuItems=new ObservableCollection<TreeViewItemModel>()
                    {
                        new TreeViewItemModel("拾色器","Color")
                    }
                },
                new TreeViewItemModel("网络工具","NetTool", "\uf17a")
                {
                     MenuItems = new ObservableCollection<TreeViewItemModel>()
                     {
                         new TreeViewItemModel("串口捕获","SerialPort"),
                         new TreeViewItemModel("网络捕获","Network"),
                         new TreeViewItemModel("TCP/UDP调试工具","NetworkDebug"),
                     }
                },
                new TreeViewItemModel("其他工具","Other", "\uf17a")
                {
                    MenuItems=new ObservableCollection<TreeViewItemModel>()
                    {
                        new TreeViewItemModel("舒尔特表","SchulteTable")
                    }
                }
            };
        }

        public string SearchText
        {
            get { return _searchText; }
            set { _searchText = value; NotifyPropertyChanged(); OnSearchTextChanged(); }
        }


        private string _searchText;

        public ObservableCollection<TreeViewItemModel> MenuItems { get; } = new ObservableCollection<TreeViewItemModel>();

        #region Event
        private void OnSearchTextChanged()
        {
            foreach (var item in MenuItems)
            {
                ChangeItemVisibility(item);
            }
        }

        private bool ChangeItemVisibility(TreeViewItemModel model)
        {
            var result = false;

            if (model.Header.ToLower().Contains(SearchText.ToLower()))
                result = true;

            if (model.MenuItems.Count != 0)
            {
                foreach (var item in model.MenuItems)
                {
                    var inner = ChangeItemVisibility(item);
                    result = result ? true : inner;
                }
            }

            model.Visibility = result ? System.Windows.Visibility.Visible : System.Windows.Visibility.Collapsed;
            model.IsExpanded = result;
            return result;
        }

        #endregion

    }
}
