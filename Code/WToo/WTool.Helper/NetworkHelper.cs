﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace WTool.Helper
{
    /// <summary>
    /// 网络帮助类
    /// </summary>
    public class NetworkHelper
    {
        /// <summary>
        /// 获取本地的所有IPV4
        /// </summary>
        /// <returns></returns>
        public static List<string> GetLocalIP()
        {
            List<string> result = new List<string>();
            string hostName = Dns.GetHostName();
            IPHostEntry ipEntry = Dns.GetHostEntry(hostName);
            foreach (var item in ipEntry.AddressList)
            {
                if (item.AddressFamily == System.Net.Sockets.AddressFamily.InterNetwork)
                {
                    result.Add(item.ToString());
                }
            }
            return result;
        }
    }
}
