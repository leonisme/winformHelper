﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using WTool.Helper.SocketHelper.Art_NetHelper.Code;

namespace WTool.Helper.SocketHelper.Art_NetHelper
{
    /// <summary>
    /// ArtNet操作
    /// </summary>
    public class ArtNetClient
    {
        #region 常量
        public const string NAME = "ArtDotNetServer";//
        public const int PORT = 6454;//监听的端口,artnet监听6454端口 
        public const string BROADCAST = "255.255.255.255";
        #endregion

        #region 委托
        [Description("监听到数据处理委托")]
        public event EventHandler<Packet.ArtNetPacket> PacketReceived;
        [Description("OpPoll(0x2000)处理委托")]
        public event EventHandler<Packet.ArtNetPacket> PollPacketReceived;
        [Description("OpDmx(0x5000)处理委托")]
        public event EventHandler<Packet.ArtNetPacket> DmxPacketReceived; 
        #endregion

        #region 变量
        IPAddress _address;//地址
        string _name;//名称
        int _port;//端口
        Net.UdpCommunicator communicator;
        #endregion

        #region 封装字段
        [Description("地址")]
        public IPAddress Address { get => _address; set => _address = value; }
        [Description("名称")]
        public string Name { get => _name; set => _name = value; }
        [Description("端口")]
        public int Port { get => _port; set => _port = value; }



        #endregion

        #region 构造方法
        /// <summary>
        /// 无参数时加载所有默认信息
        /// </summary>
        public ArtNetClient() : this(NAME) { }

        /// <summary>
        /// 名称参数存在时加载除名称外其他所有默认
        /// </summary>
        /// <param name="name"></param>
        public ArtNetClient(string name) : this(name, IPAddress.Any, PORT) { }

        /// <summary>
        /// 所有参数都不为空的情况下加载所有
        /// </summary>
        /// <param name="name"></param>
        /// <param name="address"></param>
        /// <param name="port"></param>
        public ArtNetClient(string name, IPAddress address, int port)
        {
            Name = name;
            Port = port;
            Address = address;
        }
        #endregion
        
        #region 方法
        /// <summary>
        /// 开启监听
        /// </summary>
        public void Start()
        {
            communicator = new Net.UdpCommunicator();
            communicator.DataReceived += Communicator_DataReceived;
            communicator.Start(Address, Port);
        }

        /// <summary>
        /// 关闭监听
        /// </summary>
        public void Stop()
        {
            communicator.Stop();
        }

        /// <summary>
        /// 发送数据
        /// </summary>
        /// <param name="pack"></param>
        public void SendData(Packet.UdpPacket pack)
        {
            communicator.Send(pack);
        }

        /// <summary>
        /// 检查包是否有效
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Communicator_DataReceived(object sender, Packet.UdpPacket e)
        {
            var packet = new Packet.ArtNetPacket(e.RemoteIP,e.Buffer);
            if (packet.IsValid)
            {
                RoutePacket(packet);
            }
        }

        /// <summary>
        /// 根据包的类型给到不同的委托处理
        /// </summary>
        /// <param name="packet"></param>
        private void RoutePacket(Packet.ArtNetPacket packet)
        {
            if (PacketReceived != null) PacketReceived(this, packet);

            switch (packet.OpCode)
            {
                case OpCode.OpPoll:
                    if (PollPacketReceived != null) PollPacketReceived(this, new Packet.ArtPollPacket(packet));
                    break;
                case OpCode.OpDmx:
                    if (DmxPacketReceived != null) DmxPacketReceived(this, new Packet.ArtDmxPacket(packet));
                    break;
            }
        } 
        #endregion

    }
}
