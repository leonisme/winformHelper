﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace WTool.Helper.SocketHelper.Art_NetHelper.Net
{
    public class UdpCommunicator
    {
        #region 委托
        [Description("消息处理事件（获取到数据后）")]
        public EventHandler<Packet.UdpPacket> DataReceived;
        #endregion

        #region 变量
        UdpClient socket;//udp套接字
        BackgroundWorker server;//另开线程
        #endregion

        #region 构造函数
        public UdpCommunicator()
        { }
        #endregion

        #region 开关
        /// <summary>
        /// 打开udp
        /// </summary>
        /// <param name="address"></param>
        /// <param name="port"></param>
        public void Start(IPAddress address, int port)
        {
            socket = new UdpClient();
            socket.EnableBroadcast = true;//设置可以发送和接收广播包
            socket.ExclusiveAddressUse = false;//设置允许多个客户端使用端口
            socket.Client.SendTimeout = 200;//设置发送超时事件为200毫秒
            socket.Client.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.ReuseAddress, true);//指定的指数值
            socket.Client.Bind(new IPEndPoint(address, port));//绑定ip及端口

            server = new BackgroundWorker();//另开线程执行
            server.DoWork += Server_DoWork;
            server.RunWorkerAsync();
        }

        /// <summary>
        /// 关闭
        /// </summary>
        public void Stop()
        {
            server.CancelAsync();//取消后台操作
            socket.Close();
        }
        #endregion

        #region 监听
        /// <summary>
        /// 监听
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void Server_DoWork(object sender, DoWorkEventArgs e)
        {
            while (true)
            {
                IPEndPoint client = new IPEndPoint(IPAddress.Any, 0);
                byte[] buffer = socket.Receive(ref client);
                if (DataReceived != null)//消息处理委托不为空则执行
                {
                    DataReceived(this, new Packet.UdpPacket(buffer, client));
                }
            }
        }
        #endregion

        #region 发送
        /// <summary>
        /// 异步发送
        /// </summary>
        /// <param name="pack"></param>
        public void Send(Packet.UdpPacket pack)
        {
            socket.SendAsync(pack.Buffer, pack.Buffer.Length, pack.RemoteIP);
        } 
        #endregion
    }
}
