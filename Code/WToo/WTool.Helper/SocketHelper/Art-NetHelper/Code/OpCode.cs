﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WTool.Helper.SocketHelper.Art_NetHelper.Code
{
    /// <summary>
    /// 操作码Art-Net4
    /// </summary>
    [Flags]
    public enum OpCode
    {
        OpPoll = 0x2000,//这是一个OpPoll数据包，没有其他数据包含在里面
        OpPollReply = 0x2100,//这是一个 ArtPollReply 包 ,包含设备状态信息
        OpDiagData = 0x2300,//诊断和数据记录的数据包。
        OpCommand = 0x2400,//用于发送基于文本的参数命令
        OpDmx = 0x5000, //这是一 ArtDmx 数据包。它包含零起始码 DMX512 信息的单一的全集
        OpNz = 0x5100,//这是一 ArtNzs 数据包。它包含非零起始码（除 RDM）DMX512 信息的单一的全集
        OpAddress = 0x6000,//这是一个 ArtAddress 分组。它包含了节点远程编程的信息
        OpInput = 0x7000,//这是一个 ArtInput 数据包，它包含 DMX 输入的许可 -禁止数据。
        OpTodRequest=0x8000,//这是一个 ArtTodRequest 数据包。 用来要求被发现的 RDM设备的表格
        OpTodData =0x8100,//这是一个 ArtTodData 数据包。  用来发送被发现的RDM 设备表格
        OpTodControl=0x8200,//这是一个 ArtTodControl 数据包。 用来发送被发现 RDM 的控制信息
        OpRdm=0x8300,//这是一个 ArtRdm 数据包。 这是一个数据包。用来发送所有非发现 RDM 信息
        OpRdmSu=0x8400,//这是一个 ArtRdmSub 数据包。用来发送压缩的， RDM子设备的数据。
    }
}
