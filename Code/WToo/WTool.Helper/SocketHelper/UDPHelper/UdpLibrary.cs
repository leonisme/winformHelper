﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace WTool.Helper.SocketHelper.UDPHelper
{
    /// <summary>
    /// udp
    /// </summary>
    public class UdpLibrary : IDisposable
    {
        #region 变量
        int _port = 5000;//监听的端口号
        UdpClient _udpClient;//udp协议
        bool _isStart = false;//是否开启监听
        #endregion

        #region 封装字段
        [Description("UDP网络服务")]
        [Category("UDP服务")]
        internal UdpClient UdpClient
        {
            get
            {
                if (_udpClient == null)
                {
                    bool success = true;
                    while (success)
                    {
                        try
                        {
                            _udpClient = new UdpClient(_port);
                            success = false;
                        }
                        catch (SocketException ex)
                        {
                            _port++;
                            if (_port > 65535)
                            {
                                success = false;
                                throw ex;
                            }
                        }
                    }
                    uint IOC_IN = 0x80000000;
                    uint IOC_VENDOR = 0x18000000;
                    uint SIO_UDP_CONNRESET = IOC_IN | IOC_VENDOR | 12;
                    _udpClient.Client.IOControl(
                        (int)SIO_UDP_CONNRESET,
                        new byte[] { Convert.ToByte(false) },
                        null);
                }
                return _udpClient;
            }
        }

        [Description("UDP监听端口")]
        [Category("UDP服务")]
        public int Port { get => _port; set => _port = value; }
        #endregion

        #region 构造函数
        /// <summary>
        /// 设置默认端口
        /// </summary>
        public UdpLibrary()
        {
            Port = 5000;
        }

        /// <summary>
        /// 设置传入端口
        /// </summary>
        /// <param name="port"></param>
        public UdpLibrary(int port)
        {
            Port = port;
        }
        #endregion

        #region 方法
        /// <summary>
        /// 打开服务
        /// </summary>
        public void Start()
        {
            if (_isStart)
            {
                return;
            }
            _isStart = true;
            ReceiveInternal();
        }

        /// <summary>
        /// 关闭
        /// </summary>
        public void Stop()
        {
            if (!_isStart) return;
            try
            {
                _isStart = false;
                UdpClient.Close();
                _udpClient = null;
            }
            catch { }
        }

        #region 监听
        /// <summary>
        /// 开始监听
        /// </summary>
        protected void ReceiveInternal()
        {
            if (!_isStart)
            {
                return;
            }
            try
            {
                UdpClient.BeginReceive(new AsyncCallback(ReceiveCallback), null);
            }
            catch (SocketException ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// 完成监听
        /// </summary>
        /// <param name="result"></param>
        private void ReceiveCallback(IAsyncResult result)
        {
            if (!_isStart)
            {
                return;
            }
            IPEndPoint remoteIP = new IPEndPoint(IPAddress.Any, 0);//设置监听端口及地址
            byte[] buffer = null;//接收到的数据
            try
            {
                buffer = UdpClient.EndReceive(result, ref remoteIP);//接收返回的数据
            }
            catch (SocketException ex)
            {
                throw ex;
            }
            finally
            {
                ReceiveInternal();//开始下一轮监听
            }
            OnReceiveData(new Event.ReceiveDataEventArgs(buffer, remoteIP)); //将获取到的数据进行处理
        }
        #endregion

        #region 发送
        /// <summary>
        /// 直接传递数据发送到对方地址
        /// </summary>
        /// <param name="buffer">需要发送的数据</param>
        /// <param name="remoteIP">对方地址</param>
        public void Send(byte[] buffer, IPEndPoint remoteIP)
        {
            SendInternal(buffer, remoteIP);
        }

        /// <summary>
        /// 开始发送
        /// </summary>
        /// <param name="buffer"></param>
        /// <param name="remoteIP"></param>
        protected void SendInternal(byte[] buffer, IPEndPoint remoteIP)
        {
            if (!_isStart)
            {
                throw new ApplicationException("UDP连接已经关闭");
            }
            try
            {
                UdpClient.BeginSend(buffer, buffer.Length, remoteIP, new AsyncCallback(SendCallback), null);
            }
            catch (SocketException ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// 完成数据发送
        /// </summary>
        /// <param name="result"></param>

        private void SendCallback(IAsyncResult result)
        {
            try
            {
                UdpClient.EndSend(result);
            }
            catch (SocketException ex)
            {
                throw ex;
            }
        }
        #endregion

        #endregion

        #region IDisposable成员实现
        /// <summary>
        /// 释放资源
        /// </summary>
        public void Dispose()
        {
            _isStart = false;
            if (UdpClient != null)
            {
                UdpClient.Close();
                _udpClient = null;
            }
        }
        #endregion

        #region 数据处理事件
        [Description("UDP消息处理事件")]
        [Category("UDP服务事件")]
        public event Event.ReceiveDataEventArgsHandle ReceiveData;

        /// <summary>
        /// 接收到数据后判断是否添加了处理事件，有则进行处理
        /// </summary>
        /// <param name="e">接收到的信息</param>
        protected virtual void OnReceiveData(Event.ReceiveDataEventArgs e)
        {
            if (ReceiveData != null)
            {
                ReceiveData(this, e);
            }
        }
        #endregion
    }
}
