﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace WTool.Helper.SocketHelper.UDPHelper.Event
{
    /// <summary>
    /// 设置事件
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e">接受到的所有信息</param>
    public delegate void ReceiveDataEventArgsHandle(object sender, ReceiveDataEventArgs e);

    /// <summary>
    /// 存储接受到的信息
    /// </summary>
    public class ReceiveDataEventArgs : EventArgs
    {
        #region 变量
        //ip地址
        IPEndPoint _remoteIP;
        //接收到的数据
        byte[] _buffer;
        #endregion

        #region 封装字段
        [Description("IP地址")]
        [Category("UDP服务")]
        public IPEndPoint RemoteIP { get => _remoteIP; set => _remoteIP = value; }

        [Description("获取到的数据")]
        [Category("UDP服务")]
        public byte[] Buffer { get => _buffer; set => _buffer = value; } 
        #endregion

        #region 构造函数
        /// <summary>
        /// 无参情况下
        /// </summary>
        public ReceiveDataEventArgs() { }

        /// <summary>
        /// 含参情况下
        /// </summary>
        /// <param name="buffer"></param>
        /// <param name="remoteIP"></param>
        public ReceiveDataEventArgs(byte[] buffer, IPEndPoint remoteIP)
        {
            RemoteIP = remoteIP;
            Buffer = buffer;
        }
        #endregion
    }
}
