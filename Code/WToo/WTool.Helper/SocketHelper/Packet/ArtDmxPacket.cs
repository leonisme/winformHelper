﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using WTool.Helper.SocketHelper.Art_NetHelper;

namespace WTool.Helper.SocketHelper.Packet
{
	public class ArtDmxPacket : ArtNetPacket
	{
        #region 构造方法
        public ArtDmxPacket(IPEndPoint endPoint, byte[] rawData) : base(endPoint, rawData) { }

        public ArtDmxPacket(ArtNetPacket packet) : base(packet.RemoteIP, packet.Buffer) { }
        #endregion

        #region 封装字段
        /// <summary>
        /// 序列字段是一个8位的数字，它被设计用来显示数据包起源的顺序。
        /// </summary>
        /// <value>The sequence number.</value>
        public byte Sequence { get { return Buffer[12]; } }

        /// <summary>
        /// 物理字段是一个数字，定义了产生数据包的物理端口。
        /// </summary>
        /// <value>The physical field.</value>
        public byte Physical { get { return Buffer[13]; } }

        /// <summary>
        /// Gets the sub uni.
        /// </summary>
        /// <value>The sub universe.</value>
        public byte SubUni { get { return Buffer[14]; } }

        /// <summary>
        /// Gets the net.
        /// </summary>
        /// <value>The net.</value>
        public byte Net { get { return Buffer[15]; } }

        /// <summary>
        /// Length字段定义数据字段中编码的字节数。
        /// </summary>
        /// <value>The length of the Data field.</value>
        public int Length { get { return Buffer.GetInt16(16); } }

        /// <summary>
        /// 数据字段包含数据槽(通道级别)。这个数组的大小由Length字段定义。
        /// </summary>
        /// <value>The dmx data.</value>
        public byte[] Data { get { return Buffer.Block(18, Length); } } 
        #endregion
    }
}
