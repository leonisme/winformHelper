﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace WTool.Helper.SocketHelper.Packet
{
	public class ArtPollPacket : ArtNetPacket
	{
        #region 构造方法
        public ArtPollPacket(IPEndPoint endPoint, byte[] rawData) : base(endPoint, rawData) { }

        public ArtPollPacket(ArtNetPacket packet) : base(packet.RemoteIP, packet.Buffer) { } 
        #endregion

        #region 封装字段
        /// <summary>
        /// 通话次数字段是编码为位字段的单个字节
        /// 位域全部控制接收包的节点的基本协议行为。
        /// </summary>
        /// <value></value>
        public byte TalkToMe { get { return Buffer[12]; } }

        /// <summary>
        /// 优先权字段指定控制器希望接收的诊断消息的最小优先级。
        /// </summary>
        /// <value>优先权</value>
        public byte Priority { get { return Buffer[13]; } } 
        #endregion
    }
}
