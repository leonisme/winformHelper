﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using WTool.Helper.SocketHelper.Art_NetHelper;
using WTool.Helper.SocketHelper.Art_NetHelper.Code;

namespace WTool.Helper.SocketHelper.Packet
{
    public class ArtNetPacket:UdpPacket
    {
        #region 变量
        readonly byte[] ARTNETID = { 0x41, 0x72, 0x74, 0x2d, 0x4e, 0x65, 0x74, 0 }; 
        #endregion

        #region 构造方法
        public ArtNetPacket(IPEndPoint endPoint, byte[] buffer) : base(buffer, endPoint) { }

        public ArtNetPacket(ArtNetPacket packet) : base(packet.Buffer, packet.RemoteIP) { }
        #endregion

        #region 封装字段
        /// <summary>
        /// 操作码标识数据包类型
        /// </summary>
        /// <value>操作码</value>
        public OpCode OpCode { get { return (OpCode)Buffer.GetInt16LE(8); } }

        /// <summary>
        /// 标识Art-Net的版本
        /// </summary>
        /// <value>Art-Net的版本</value>
        public int ProtocolVersion { get { return Buffer.GetInt16(10); } }

        /// <summary>
        /// 检查包是否有效
        /// </summary>
        /// <value>包是否有效</value>
        public bool IsValid { get { return ARTNETID.SequenceEqual(Buffer.Block(0, ARTNETID.Length)); } } 
        #endregion
    }
}
