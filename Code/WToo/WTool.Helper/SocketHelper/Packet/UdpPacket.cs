﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace WTool.Helper.SocketHelper.Packet
{
    /// <summary>
    /// udp接收的包
    /// 必须有参
    /// </summary>
    public class UdpPacket
    {
        #region 变量
        IPEndPoint _remoteIP;//IP地址
        byte[] _buffer;//接收到的数据包
        #endregion

        #region 封装字段
        [Description("IP地址")]
        [Category("UDP服务")]
        public IPEndPoint RemoteIP { get => _remoteIP;private set => _remoteIP = value; }
        [Description("接收到的数据包")]
        [Category("UDP服务")]
        public byte[] Buffer { get => _buffer;private set => _buffer = value; }
        #endregion

        #region 构造函数
        /// <summary>
        /// 含参情况下
        /// </summary>
        /// <param name="buffer"></param>
        /// <param name="remoteIP"></param>
        public UdpPacket(byte[] buffer, IPEndPoint remoteIP)
        {
            RemoteIP = remoteIP;
            Buffer = buffer;
        } 
        #endregion
    }
}
