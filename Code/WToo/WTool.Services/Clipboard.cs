﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WTool.IRepository;
using WTool.IServices;
using WTool.Repository.Database;

namespace WTool.Services
{
    /// <summary>
    /// 粘贴板服务接口实现
    /// </summary>
    public class Clipboard : IClipboard
    {
        IDatabase db = new SqliteHelper();
        /// <summary>
        /// 实现添加粘贴板数据
        /// </summary>
        /// <param name="content"></param>
        /// <returns></returns>
        public bool InsertClipboard(string content)
        {
            string sql = $"insert into Clipboard values('{Guid.NewGuid().ToString()}','{content}','{DateTime.Now}')";
            if (db.ExecuteNonQuery(sql, CommandType.Text) > 0)
            {
                return true;
            }
            return false;
        }

        /// <summary>
        /// 实现查询指定数量粘贴板数据
        /// </summary>
        /// <param name="count"></param>
        /// <returns></returns>
        public DataTable CreateCent(string count)
        {
            return db.ExecuteDataTable($"SELECT * FROM (SELECT * FROM Clipboard LIMIT {count} offset (SELECT COUNT(*) - {count} FROM Clipboard)) as tb1 ORDER BY DataTime desc");
        }

        /// <summary>
        /// 清除粘贴板所有信息
        /// </summary>
        /// <returns></returns>
        public int ClearData()
        {
            return db.ExecuteNonQuery("delete from Clipboard");
        }
    }
}
