﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Diagnostics;
using System.Threading;
using System.Windows.Forms;

namespace WToo.Views.WindowsTool
{
    public partial class FrmWinCPU : Form
    {
        PerformanceCounter[] counters;

        public FrmWinCPU()
        {
            InitializeComponent();
            // 初始化计数器
            counters = new PerformanceCounter[System.Environment.ProcessorCount];
            for (int i = 0; i < counters.Length; i++)
            {
                counters[i] = new PerformanceCounter("Processor", "% Processor Time", i.ToString());
                counters[i].NextValue(); // 这里是为了获得CPU占用率的值
            }
        }

        // 返回所有核心的CPU的占用率的值
        public double[] GetCPUUsage()
        {
            double[] info = new double[counters.Length];
            for (int i = 0; i < counters.Length; i++)
                info[i] = counters[i].NextValue();
            return info;
        }

        /// <summary>
        /// 退出
        /// </summary>
        private void FrmWinCPU_FormClosed(object sender, FormClosedEventArgs e)
        {
            Helper.FormExitHelper.Exit();
        }
        Queue que = new Queue();//队列存储往期值
        private string GetCPU()
        {
            double[] dou = GetCPUUsage();
            string s = $"CPU使用率({dou.Length}核)：";
            for (int i = 0; i < dou.Length; i++)
            {
                s += $"    {i + 1}CPU：{Math.Round(dou[i], 2)}";
            }
            que.Enqueue(s);//入队
            return s;
        }

        private void GetSystem_Tick(object sender, EventArgs e)
        {
            label1.Text = GetCPU();
        }

        private void FrmWinCPU_Load(object sender, EventArgs e)
        {
            GetSystem.Start();
        }
    }
}
