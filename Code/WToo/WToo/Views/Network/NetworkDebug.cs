﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WTool.Helper;
using WTool.Helper.SocketHelper.UDPHelper;
using WTool.Helper.SocketHelper.UDPHelper.Event;

namespace WToo.Views.Network
{
    public partial class NetworkDebug : Form
    {
        public NetworkDebug()
        {
            InitializeComponent();
        }

        UdpLibrary udp = new UdpLibrary();

        /// <summary>
        /// 关闭窗体
        /// </summary>
        private void NetworkDebug_FormClosed(object sender, FormClosedEventArgs e)
        {
            Helper.FormExitHelper.Exit();
        }

        private void NetworkDebug_Load(object sender, EventArgs e)
        {
            List<string> ips = NetworkHelper.GetLocalIP();
            cmb_ip.DataSource = ips;
        }

        /// <summary>
        /// 打开连接
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_open_Click(object sender, EventArgs e)
        {
            try
            {
                if (cmb_mode.Text == "UDP")
                {
                    if (btn_open.Text == "停止")
                    {
                        udp.Stop();
                        btn_open.Text = "打开";
                        EnabledControls(true);
                    }
                    else
                    {
                        if (num_port.Value != 0)
                            udp.Port = (int)num_port.Value;
                        udp.ReceiveData += EventUDPListing;
                        udp.Start();
                        btn_open.Text = "停止";
                        EnabledControls(false);
                    }
                }
            }
            catch
            {
                MessageBox.Show("参数输入有误！请检查");
            }
        }

        /// <summary>
        /// 发送
        /// </summary>
        private void btn_send_Click(object sender, EventArgs e)
        {
            if (cmb_mode.Text == "UDP")
            {
                byte[] sendMess = null;
                string[] send = txt_send.Text.Split(' ');
                sendMess = new byte[send.Length];
                for (int i = 0; i < send.Length; i++)
                {
                    if (!string.IsNullOrEmpty(send[i]))
                    {
                        sendMess[i] = byte.Parse(send[i], System.Globalization.NumberStyles.HexNumber);
                    }
                }
                if (sendMess != null)
                {
                    IPEndPoint end = new IPEndPoint(IPAddress.Parse(txt_it_ip.Text), (int)num_it_port.Value);
                    udp.Send(sendMess, end);
                }
            }
        }

        /// <summary>
        /// UDP监听事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void EventUDPListing(object sender, ReceiveDataEventArgs e)
        {
            StringBuilder strListing = new StringBuilder();
            foreach (byte item in e.Buffer)
            {
                strListing.Append(item.ToString("X2")+" ");
            }
            this.BeginInvoke(new Action(() =>
            {
                txt_listing.Text = strListing.ToString()+ "\r\t";
            }));
        }

        /// <summary>
        /// 统一修改控件是否可用
        /// </summary>
        /// <param name="isEnabled">是否可用</param>
        private void EnabledControls(bool isEnabled)
        {
            cmb_mode.Enabled = isEnabled;
            num_port.Enabled = isEnabled;
            txt_it_ip.Enabled = isEnabled;
            num_it_port.Enabled = isEnabled;
            cmb_ip.Enabled = isEnabled;
        }


    }
}
