﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WTool.Helper;

namespace WToo.Views.FrmTheColor
{
    public partial class BtnTheColor : Form
    {
        public BtnTheColor()
        {
            InitializeComponent();
        }
        #region 初始化
        private void BtnTheColor_Load(object sender, EventArgs e)
        {
            InitFont();
        }

        /// <summary>
        /// 初始化界面字体
        /// </summary>
        private void InitFont()
        {
            Font myfont = Helper.UIHelper.GetFont(10);
            btnSelect.Font = myfont;
            btnGet.Font = myfont;
        }
        #endregion

        #region 外部API（窗体拖动）
        [DllImport("user32.dll")]
        public static extern bool ReleaseCapture();

        [DllImport("user32.dll")]
        public static extern bool SendMessage(IntPtr hwnd, int wMsg, int wParam, int lParam);
        #endregion

        #region 事件
        #region 右上角系统操作
        /// <summary>
        /// 关闭
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void UBtnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        /// <summary>
        /// 最小化
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void UBtnMinimize_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        /// <summary>
        /// 锁定
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        bool isLock = false;//是否锁定
        private void uiButton1_Click(object sender, EventArgs e)
        {
            if (isLock)
            { //解锁
                isLock = false;
                uiButton1.RectColor = Color.FromArgb(128, 255, 128);
            }
            else
            { //加锁
                isLock = true;
                uiButton1.RectColor = Color.FromArgb(192, 0, 0);
            }
            this.TopMost = isLock;
        }
        #endregion

        /// <summary>
        /// 设置窗体可拖动
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnTheColor_MouseDown(object sender, MouseEventArgs e)
        {
            ReleaseCapture();
            SendMessage(this.Handle, 0x0112, 0xF012, 0);
        }

        /// <summary>
        /// 屏幕取色
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uiButton2_Click(object sender, EventArgs e)
        {
            TheColorScreen frm = new TheColorScreen();
            if (frm.ShowDialog() == DialogResult.OK)
            {
                Color color = frm.nowColor;
                txtValue.Text = ColorTranslator.ToHtml(color);
                Clipboard.SetText(ColorTranslator.ToHtml(color));
                txtValue.Text += $"/{color.R},{color.G},{color.B}";
            }
        }

        /// <summary>
        /// 退出时执行
        /// </summary>
        private void BtnTheColor_FormClosed(object sender, FormClosedEventArgs e)
        {
            Helper.FormExitHelper.Exit();
        }


        private void btnSelect_Click(object sender, EventArgs e)
        {
            if (colorDialog.ShowDialog() == DialogResult.OK)
            {
                txtValue.Text = ColorTranslator.ToHtml(colorDialog.Color);
                Clipboard.SetText(ColorTranslator.ToHtml(colorDialog.Color));
                txtValue.Text += $"/{colorDialog.Color.R},{colorDialog.Color.G},{colorDialog.Color.B}";
            }
        }


        #endregion


    }
}
