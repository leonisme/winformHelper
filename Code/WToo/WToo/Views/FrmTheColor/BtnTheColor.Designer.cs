﻿namespace WToo.Views.FrmTheColor
{
    partial class BtnTheColor
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.uiButton1 = new Sunny.UI.UIButton();
            this.UBtnMinimize = new Sunny.UI.UIButton();
            this.UBtnClose = new Sunny.UI.UIButton();
            this.btnGet = new Sunny.UI.UIButton();
            this.txtValue = new Sunny.UI.UITextBox();
            this.btnSelect = new Sunny.UI.UIButton();
            this.colorDialog = new System.Windows.Forms.ColorDialog();
            this.SuspendLayout();
            // 
            // uiButton1
            // 
            this.uiButton1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.uiButton1.BackColor = System.Drawing.Color.Transparent;
            this.uiButton1.BackgroundImage = global::WToo.Properties.Resources._lock;
            this.uiButton1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.uiButton1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.uiButton1.FillColor = System.Drawing.Color.Transparent;
            this.uiButton1.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.uiButton1.Location = new System.Drawing.Point(65, 1);
            this.uiButton1.MinimumSize = new System.Drawing.Size(1, 1);
            this.uiButton1.Name = "uiButton1";
            this.uiButton1.RectColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.uiButton1.Size = new System.Drawing.Size(20, 20);
            this.uiButton1.Style = Sunny.UI.UIStyle.Custom;
            this.uiButton1.TabIndex = 2;
            this.uiButton1.Click += new System.EventHandler(this.uiButton1_Click);
            // 
            // UBtnMinimize
            // 
            this.UBtnMinimize.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.UBtnMinimize.BackColor = System.Drawing.Color.Transparent;
            this.UBtnMinimize.BackgroundImage = global::WToo.Properties.Resources.Minimize;
            this.UBtnMinimize.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.UBtnMinimize.Cursor = System.Windows.Forms.Cursors.Hand;
            this.UBtnMinimize.FillColor = System.Drawing.Color.Transparent;
            this.UBtnMinimize.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.UBtnMinimize.Location = new System.Drawing.Point(91, 1);
            this.UBtnMinimize.MinimumSize = new System.Drawing.Size(1, 1);
            this.UBtnMinimize.Name = "UBtnMinimize";
            this.UBtnMinimize.RectColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.UBtnMinimize.Size = new System.Drawing.Size(20, 20);
            this.UBtnMinimize.Style = Sunny.UI.UIStyle.Custom;
            this.UBtnMinimize.TabIndex = 1;
            this.UBtnMinimize.Click += new System.EventHandler(this.UBtnMinimize_Click);
            // 
            // UBtnClose
            // 
            this.UBtnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.UBtnClose.BackColor = System.Drawing.Color.Transparent;
            this.UBtnClose.BackgroundImage = global::WToo.Properties.Resources.close;
            this.UBtnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.UBtnClose.Cursor = System.Windows.Forms.Cursors.Hand;
            this.UBtnClose.FillColor = System.Drawing.Color.Transparent;
            this.UBtnClose.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.UBtnClose.Location = new System.Drawing.Point(117, 1);
            this.UBtnClose.MinimumSize = new System.Drawing.Size(1, 1);
            this.UBtnClose.Name = "UBtnClose";
            this.UBtnClose.RectColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.UBtnClose.Size = new System.Drawing.Size(20, 20);
            this.UBtnClose.Style = Sunny.UI.UIStyle.Custom;
            this.UBtnClose.TabIndex = 0;
            this.UBtnClose.Click += new System.EventHandler(this.UBtnClose_Click);
            // 
            // btnGet
            // 
            this.btnGet.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.btnGet.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnGet.FillColor = System.Drawing.Color.White;
            this.btnGet.FillHoverColor = System.Drawing.Color.White;
            this.btnGet.FillPressColor = System.Drawing.Color.White;
            this.btnGet.FillSelectedColor = System.Drawing.Color.White;
            this.btnGet.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btnGet.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
            this.btnGet.ForeHoverColor = System.Drawing.Color.Black;
            this.btnGet.ForePressColor = System.Drawing.Color.Black;
            this.btnGet.ForeSelectedColor = System.Drawing.Color.Black;
            this.btnGet.Location = new System.Drawing.Point(11, 63);
            this.btnGet.MinimumSize = new System.Drawing.Size(1, 1);
            this.btnGet.Name = "btnGet";
            this.btnGet.RectColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(204)))), ((int)(((byte)(255)))));
            this.btnGet.RectHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(153)))), ((int)(((byte)(153)))), ((int)(((byte)(204)))));
            this.btnGet.RectPressColor = System.Drawing.Color.FromArgb(((int)(((byte)(153)))), ((int)(((byte)(102)))), ((int)(((byte)(153)))));
            this.btnGet.RectSelectedColor = System.Drawing.Color.FromArgb(((int)(((byte)(153)))), ((int)(((byte)(153)))), ((int)(((byte)(204)))));
            this.btnGet.Size = new System.Drawing.Size(126, 31);
            this.btnGet.Style = Sunny.UI.UIStyle.Custom;
            this.btnGet.TabIndex = 3;
            this.btnGet.Text = "屏幕取色";
            this.btnGet.Click += new System.EventHandler(this.uiButton2_Click);
            // 
            // txtValue
            // 
            this.txtValue.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.txtValue.BackColor = System.Drawing.Color.Transparent;
            this.txtValue.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txtValue.FillColor = System.Drawing.Color.White;
            this.txtValue.Font = new System.Drawing.Font("微软雅黑", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txtValue.Location = new System.Drawing.Point(11, 93);
            this.txtValue.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtValue.Maximum = 2147483647D;
            this.txtValue.Minimum = -2147483648D;
            this.txtValue.MinimumSize = new System.Drawing.Size(1, 1);
            this.txtValue.Name = "txtValue";
            this.txtValue.Padding = new System.Windows.Forms.Padding(5);
            this.txtValue.RectColor = System.Drawing.Color.Transparent;
            this.txtValue.Size = new System.Drawing.Size(125, 26);
            this.txtValue.Style = Sunny.UI.UIStyle.Custom;
            this.txtValue.TabIndex = 5;
            // 
            // btnSelect
            // 
            this.btnSelect.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.btnSelect.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnSelect.FillColor = System.Drawing.Color.White;
            this.btnSelect.FillHoverColor = System.Drawing.Color.White;
            this.btnSelect.FillPressColor = System.Drawing.Color.White;
            this.btnSelect.FillSelectedColor = System.Drawing.Color.White;
            this.btnSelect.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btnSelect.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
            this.btnSelect.ForeHoverColor = System.Drawing.Color.Black;
            this.btnSelect.ForePressColor = System.Drawing.Color.Black;
            this.btnSelect.ForeSelectedColor = System.Drawing.Color.Black;
            this.btnSelect.Location = new System.Drawing.Point(11, 33);
            this.btnSelect.MinimumSize = new System.Drawing.Size(1, 1);
            this.btnSelect.Name = "btnSelect";
            this.btnSelect.RectColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(204)))), ((int)(((byte)(255)))));
            this.btnSelect.RectHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(153)))), ((int)(((byte)(153)))), ((int)(((byte)(204)))));
            this.btnSelect.RectPressColor = System.Drawing.Color.FromArgb(((int)(((byte)(153)))), ((int)(((byte)(102)))), ((int)(((byte)(153)))));
            this.btnSelect.RectSelectedColor = System.Drawing.Color.FromArgb(((int)(((byte)(153)))), ((int)(((byte)(153)))), ((int)(((byte)(204)))));
            this.btnSelect.Size = new System.Drawing.Size(126, 31);
            this.btnSelect.Style = Sunny.UI.UIStyle.Custom;
            this.btnSelect.TabIndex = 6;
            this.btnSelect.Text = "颜色选择";
            this.btnSelect.Click += new System.EventHandler(this.btnSelect_Click);
            // 
            // BtnTheColor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.SkyBlue;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.ClientSize = new System.Drawing.Size(145, 133);
            this.Controls.Add(this.btnSelect);
            this.Controls.Add(this.txtValue);
            this.Controls.Add(this.btnGet);
            this.Controls.Add(this.uiButton1);
            this.Controls.Add(this.UBtnMinimize);
            this.Controls.Add(this.UBtnClose);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "BtnTheColor";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "BtnTheColor";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.BtnTheColor_FormClosed);
            this.Load += new System.EventHandler(this.BtnTheColor_Load);
            this.MouseDown += new System.Windows.Forms.MouseEventHandler(this.BtnTheColor_MouseDown);
            this.ResumeLayout(false);

        }

        #endregion

        private Sunny.UI.UIButton UBtnClose;
        private Sunny.UI.UIButton UBtnMinimize;
        private Sunny.UI.UIButton uiButton1;
        private Sunny.UI.UIButton btnGet;
        private Sunny.UI.UITextBox txtValue;
        private Sunny.UI.UIButton btnSelect;
        private System.Windows.Forms.ColorDialog colorDialog;
    }
}