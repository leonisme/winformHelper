﻿using CCWin;
using System;
using System.Data;
using System.Drawing;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using WTool.Helper;
using WTool.IServices;

namespace WToo
{
    public partial class FrmClipboard : CCSkinMain
    {
        Controls.Clipboard control = new Controls.Clipboard();
        public FrmClipboard()
        {
            InitializeComponent();
            nextClipboardViewer = (IntPtr)SetClipboardViewer((int)this.Handle);
            //CreateCent();
            control.CreateCent(skinPanel, ScbCount.Text);
        }

        IClipboard clipboard = new WTool.Services.Clipboard();//粘贴板服务

        #region 提取粘贴板信息
        [DllImport("User32.dll")]
        protected static extern int SetClipboardViewer(int hWndNewViewer);

        [DllImport("User32.dll", CharSet = CharSet.Auto)]
        public static extern bool ChangeClipboardChain(IntPtr hWndRemove, IntPtr hWndNewNext);

        [DllImport("user32.dll", CharSet = CharSet.Auto)]
        public static extern int SendMessage(IntPtr hwnd, int wMsg, IntPtr wParam, IntPtr lParam);
        IntPtr nextClipboardViewer;

        /// <summary>
        /// 处理windows消息
        /// </summary>
        /// <param name="m"></param>
        protected override void WndProc(ref System.Windows.Forms.Message m)
        {
            //  defined in winuser.h
            const int WM_DRAWCLIPBOARD = 0x308;
            const int WM_CHANGECBCHAIN = 0x030D;

            switch (m.Msg)
            {
                case WM_DRAWCLIPBOARD:
                    control.DisplayClipboardData(skinPanel, ScbCount.Text);
                    SendMessage(nextClipboardViewer, m.Msg, m.WParam,
                                m.LParam);
                    break;

                case WM_CHANGECBCHAIN:
                    if (m.WParam == nextClipboardViewer)
                        nextClipboardViewer = m.LParam;
                    else
                        SendMessage(nextClipboardViewer, m.Msg, m.WParam,
                                    m.LParam);
                    break;
                default:
                    base.WndProc(ref m);
                    break;
            }
        }
        #endregion

        #region 事件
        /// <summary>
        /// 清空粘贴板
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SBtnClear_Click(object sender, System.EventArgs e)
        {
            control.ClearClipboard(skinPanel);
        }

        /// <summary>
        /// 每页显示页数
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ScbCount_SelectedIndexChanged(object sender, EventArgs e)
        {
            control.CreateCent(skinPanel, ScbCount.Text);
        }

        /// <summary>
        /// 关闭窗体
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void FrmClipboard_FormClosing(object sender, FormClosingEventArgs e)
        {
            Helper.FormExitHelper.Exit();
        }
        #endregion

        #region 选项操作
        /// <summary>
        /// 固定或者非固定
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ScbIsFixation_CheckedChanged(object sender, System.EventArgs e)
        {
            this.TopMost = ScbIsFixation.Checked ? true : false;
        }

        /// <summary>
        /// 是否在任务栏显示
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ScbIsTaskbar_CheckedChanged(object sender, System.EventArgs e)
        {
            this.ShowInTaskbar = ScbIsTaskbar.Checked ? true : false;
        }
        #endregion

       
    }
}
