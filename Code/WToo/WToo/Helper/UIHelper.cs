﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Text;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WToo.Helper
{
    /// <summary>
    /// 跟UI相关的帮助方法
    /// </summary>
    public static class UIHelper
    {
        /// <summary>
        /// 获取字体
        /// </summary>
        /// <param name="size">字体大小</param>
        /// <returns></returns>
        public static Font GetFont(int size)
        {
            string fontSour = Application.StartupPath + "\\Fonts\\HarmonyOS_Sans_SC_Regular.ttf";
            PrivateFontCollection font = new PrivateFontCollection();
            font.AddFontFile(fontSour);//字体的路径及名字
            Font myFont = new Font(font.Families[0].Name, size, FontStyle.Regular, GraphicsUnit.Point, ((byte)(134)));
            return myFont;
        }

        /// <summary>
        /// 绘制圆角矩形
        /// </summary>
        /// <param name="rect">矩形区域</param>
        /// <param name="radius">圆角程度</param>
        /// <returns></returns>
        public static GraphicsPath GetRoundedRectPath(Rectangle rect, int radius)
        {
            int diameter = radius;
            Rectangle arcRect = new Rectangle(rect.Location, new Size(diameter, diameter));
            GraphicsPath path = new GraphicsPath();

            // 左上角
            path.AddArc(arcRect, 180, 90);

            // 右上角
            arcRect.X = rect.Right - diameter;
            path.AddArc(arcRect, 270, 90);

            // 右下角
            arcRect.Y = rect.Bottom - diameter;
            path.AddArc(arcRect, 0, 90);

            // 左下角
            arcRect.X = rect.Left;
            path.AddArc(arcRect, 90, 90);
            path.CloseFigure();//闭合曲线
            return path;
        }
    }
}
