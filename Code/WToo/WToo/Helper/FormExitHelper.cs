﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WTool.Model.Base;

namespace WToo.Helper
{
    /// <summary>
    /// 窗口关闭状态
    /// </summary>
    public static class FormExitHelper
    {
        /// <summary>
        /// 是否可以关闭
        /// </summary>
        public static void Exit()
        {
            Configuration config = Configuration.GetConfiguration;
            if (config.IsExit)
            {
                if (Application.OpenForms.Count < 2)
                {
                    Application.ExitThread();
                }
            }
        }
    }
}
