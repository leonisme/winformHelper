﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WToo
{
    public partial class FrmMain : Form
    {
        #region 初始化
        public FrmMain()
        {
            InitializeComponent();
        }

        private void FrmMain_Load(object sender, EventArgs e)
        {
            InitFont();
            this.Region = new Region(Helper.UIHelper.GetRoundedRectPath(new Rectangle(0, 0, this.Width, this.Height), 10));
        }

        private void InitFont()
        {
            Font myfont = Helper.UIHelper.GetFont(18);
            labTitle.Font = Helper.UIHelper.GetFont(20);
            labClipboard.Font = myfont;
            labTheColor.Font = myfont;
            labWinCpu.Font = myfont;
        }
        #endregion

        #region 外部api（拖动）
        [DllImport("user32.dll")]
        public static extern bool ReleaseCapture();

        [DllImport("user32.dll")]
        public static extern bool SendMessage(IntPtr hwnd, int wMsg, int wParam, int lParam);
        #endregion

        #region 方法

        #endregion

        #region 事件


        #region 退出按钮

        private void labExit_MouseEnter(object sender, EventArgs e)
        {
            labExit.Image = Properties.Resources.exitEnter;
        }

        private void labExit_MouseLeave(object sender, EventArgs e)
        {
            labExit.Image = Properties.Resources.exitOut;
        }

        /// <summary>
        /// 退出操作
        /// 如果打开了子窗体的情况下会隐藏自身
        /// 如果没有任何子窗体打开将关闭所有线程
        /// </summary>
        private void labExit_Click(object sender, EventArgs e)
        {
            Helper.FormExitHelper.Exit();
            this.Hide();
        }

        #endregion

        /// <summary>
        /// 打开粘贴板
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void labClipboard_Click(object sender, EventArgs e)
        {
            FrmClipboard form = new FrmClipboard();
            form.Show();
        }

        /// <summary>
        /// 拾色器
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void labTheColor_Click(object sender, EventArgs e)
        {
            Views.FrmTheColor.BtnTheColor form = new Views.FrmTheColor.BtnTheColor();
            form.Show();
        }

        /// <summary>
        /// CPU
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void labWinCpu_Click(object sender, EventArgs e)
        {
            Views.WindowsTool.FrmWinCPU form = new Views.WindowsTool.FrmWinCPU();
            form.Show();
        }

        /// <summary>
        /// 网络调试工具
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void lab_network_Click(object sender, EventArgs e)
        {
            Views.Network.NetworkDebug frm = new Views.Network.NetworkDebug();
            frm.Show();
        }

        /// <summary>
        /// 拖动
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void panTop_MouseDown(object sender, MouseEventArgs e)
        {
            ReleaseCapture();
            SendMessage(this.Handle, 0x0112, 0xF012, 0);
        }


        #region 下方工具按钮移入移出效果
        private void labControls_MouseEnter(object sender, EventArgs e)
        {
            Label lab = sender as Label;
            lab.Image = Properties.Resources.Clipboard;
        }

        private void labControls_MouseLeave(object sender, EventArgs e)
        {
            Label lab = sender as Label;
            lab.Image = Properties.Resources.btnEnter;
        }




        #endregion

        #endregion


    }
}
