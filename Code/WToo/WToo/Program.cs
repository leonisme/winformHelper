﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using WToo.Views.WindowsTool;

namespace WToo
{
    static class Program
    {
        /// <summary>
        /// 应用程序的主入口点。
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.SetUnhandledExceptionMode(UnhandledExceptionMode.CatchException);
            Application.ThreadException += new ThreadExceptionEventHandler(ApplicationThreadException);
            AppDomain.CurrentDomain.UnhandledException += new UnhandledExceptionEventHandler(AppDomainUnhandledException);
            isEx = true;//标志程序可以退出
            Application.Run(new FrmMain());
            //Application.Run(new Views.Network.NetworkDebug());
        }

        /// <summary>
        /// UI发生错误时处理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private static void ApplicationThreadException(object sender, ThreadExceptionEventArgs e)
        {
            try
            {
                MessageBox.Show($"UI处理发生异常：{e.Exception}");
            }
            catch { }
        }

        static bool isEx = false;//是否退出应用
        /// <summary>
        /// 线程发生错误时(致命异常)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private static void AppDomainUnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            try
            {
                Console.WriteLine(e.ExceptionObject);
            }
            catch { }
        }
    }
}
