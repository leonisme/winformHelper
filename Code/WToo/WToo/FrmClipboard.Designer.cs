﻿namespace WToo
{
    partial class FrmClipboard
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            ChangeClipboardChain(this.Handle, nextClipboardViewer);
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.SBtnClear = new CCWin.SkinControl.SkinButton();
            this.ScbIsFixation = new CCWin.SkinControl.SkinCheckBox();
            this.skinPanel = new CCWin.SkinControl.SkinPanel();
            this.ScbIsTaskbar = new CCWin.SkinControl.SkinCheckBox();
            this.skinLine1 = new CCWin.SkinControl.SkinLine();
            this.skinPanel1 = new CCWin.SkinControl.SkinPanel();
            this.label1 = new System.Windows.Forms.Label();
            this.ScbCount = new CCWin.SkinControl.SkinComboBox();
            this.skinPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // SBtnClear
            // 
            this.SBtnClear.BackColor = System.Drawing.Color.Transparent;
            this.SBtnClear.BaseColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.SBtnClear.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.SBtnClear.ControlState = CCWin.SkinClass.ControlState.Normal;
            this.SBtnClear.DownBack = null;
            this.SBtnClear.Location = new System.Drawing.Point(316, 12);
            this.SBtnClear.MouseBack = null;
            this.SBtnClear.Name = "SBtnClear";
            this.SBtnClear.NormlBack = null;
            this.SBtnClear.Size = new System.Drawing.Size(54, 38);
            this.SBtnClear.TabIndex = 0;
            this.SBtnClear.Text = "清空";
            this.SBtnClear.UseVisualStyleBackColor = false;
            this.SBtnClear.Click += new System.EventHandler(this.SBtnClear_Click);
            // 
            // ScbIsFixation
            // 
            this.ScbIsFixation.AutoSize = true;
            this.ScbIsFixation.BackColor = System.Drawing.Color.Transparent;
            this.ScbIsFixation.Checked = true;
            this.ScbIsFixation.CheckState = System.Windows.Forms.CheckState.Checked;
            this.ScbIsFixation.ControlState = CCWin.SkinClass.ControlState.Normal;
            this.ScbIsFixation.DownBack = null;
            this.ScbIsFixation.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.ScbIsFixation.Location = new System.Drawing.Point(39, 12);
            this.ScbIsFixation.MouseBack = null;
            this.ScbIsFixation.Name = "ScbIsFixation";
            this.ScbIsFixation.NormlBack = null;
            this.ScbIsFixation.SelectedDownBack = null;
            this.ScbIsFixation.SelectedMouseBack = null;
            this.ScbIsFixation.SelectedNormlBack = null;
            this.ScbIsFixation.Size = new System.Drawing.Size(75, 21);
            this.ScbIsFixation.TabIndex = 1;
            this.ScbIsFixation.Text = "是否固定";
            this.ScbIsFixation.UseVisualStyleBackColor = false;
            this.ScbIsFixation.CheckedChanged += new System.EventHandler(this.ScbIsFixation_CheckedChanged);
            // 
            // skinPanel
            // 
            this.skinPanel.AutoScroll = true;
            this.skinPanel.BackColor = System.Drawing.Color.Transparent;
            this.skinPanel.ControlState = CCWin.SkinClass.ControlState.Normal;
            this.skinPanel.DownBack = null;
            this.skinPanel.Location = new System.Drawing.Point(7, 31);
            this.skinPanel.MouseBack = null;
            this.skinPanel.Name = "skinPanel";
            this.skinPanel.NormlBack = null;
            this.skinPanel.Size = new System.Drawing.Size(361, 154);
            this.skinPanel.TabIndex = 2;
            // 
            // ScbIsTaskbar
            // 
            this.ScbIsTaskbar.AutoSize = true;
            this.ScbIsTaskbar.BackColor = System.Drawing.Color.Transparent;
            this.ScbIsTaskbar.Checked = true;
            this.ScbIsTaskbar.CheckState = System.Windows.Forms.CheckState.Checked;
            this.ScbIsTaskbar.ControlState = CCWin.SkinClass.ControlState.Normal;
            this.ScbIsTaskbar.DownBack = null;
            this.ScbIsTaskbar.Enabled = false;
            this.ScbIsTaskbar.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.ScbIsTaskbar.Location = new System.Drawing.Point(39, 36);
            this.ScbIsTaskbar.MouseBack = null;
            this.ScbIsTaskbar.Name = "ScbIsTaskbar";
            this.ScbIsTaskbar.NormlBack = null;
            this.ScbIsTaskbar.SelectedDownBack = null;
            this.ScbIsTaskbar.SelectedMouseBack = null;
            this.ScbIsTaskbar.SelectedNormlBack = null;
            this.ScbIsTaskbar.Size = new System.Drawing.Size(123, 21);
            this.ScbIsTaskbar.TabIndex = 3;
            this.ScbIsTaskbar.Text = "是否在任务栏显示";
            this.ScbIsTaskbar.UseVisualStyleBackColor = false;
            this.ScbIsTaskbar.CheckedChanged += new System.EventHandler(this.ScbIsTaskbar_CheckedChanged);
            // 
            // skinLine1
            // 
            this.skinLine1.BackColor = System.Drawing.Color.Transparent;
            this.skinLine1.LineColor = System.Drawing.Color.Black;
            this.skinLine1.LineHeight = 1;
            this.skinLine1.Location = new System.Drawing.Point(-1, 5);
            this.skinLine1.Name = "skinLine1";
            this.skinLine1.Size = new System.Drawing.Size(480, 10);
            this.skinLine1.TabIndex = 0;
            this.skinLine1.Text = "skinLine1";
            // 
            // skinPanel1
            // 
            this.skinPanel1.BackColor = System.Drawing.Color.Transparent;
            this.skinPanel1.Controls.Add(this.label1);
            this.skinPanel1.Controls.Add(this.ScbCount);
            this.skinPanel1.Controls.Add(this.skinLine1);
            this.skinPanel1.Controls.Add(this.SBtnClear);
            this.skinPanel1.Controls.Add(this.ScbIsTaskbar);
            this.skinPanel1.Controls.Add(this.ScbIsFixation);
            this.skinPanel1.ControlState = CCWin.SkinClass.ControlState.Normal;
            this.skinPanel1.DownBack = null;
            this.skinPanel1.Location = new System.Drawing.Point(-32, 184);
            this.skinPanel1.MouseBack = null;
            this.skinPanel1.Name = "skinPanel1";
            this.skinPanel1.NormlBack = null;
            this.skinPanel1.Size = new System.Drawing.Size(421, 71);
            this.skinPanel1.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(178, 21);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(53, 12);
            this.label1.TabIndex = 5;
            this.label1.Text = "显示数：";
            // 
            // ScbCount
            // 
            this.ScbCount.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.ScbCount.FormattingEnabled = true;
            this.ScbCount.Items.AddRange(new object[] {
            "10",
            "20",
            "30",
            "40",
            "50"});
            this.ScbCount.Location = new System.Drawing.Point(237, 16);
            this.ScbCount.Name = "ScbCount";
            this.ScbCount.Size = new System.Drawing.Size(42, 22);
            this.ScbCount.TabIndex = 4;
            this.ScbCount.Text = "10";
            this.ScbCount.WaterText = "";
            this.ScbCount.SelectedIndexChanged += new System.EventHandler(this.ScbCount_SelectedIndexChanged);
            // 
            // FrmClipboard
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Gainsboro;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.CanResize = false;
            this.ClientSize = new System.Drawing.Size(375, 251);
            this.Controls.Add(this.skinPanel1);
            this.Controls.Add(this.skinPanel);
            this.MaximizeBox = false;
            this.Name = "FrmClipboard";
            this.Shadow = false;
            this.ShowBorder = false;
            this.Text = "粘贴板";
            this.TopMost = true;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FrmClipboard_FormClosing);
            this.skinPanel1.ResumeLayout(false);
            this.skinPanel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private CCWin.SkinControl.SkinButton SBtnClear;
        private CCWin.SkinControl.SkinCheckBox ScbIsFixation;
        private CCWin.SkinControl.SkinPanel skinPanel;
        private CCWin.SkinControl.SkinCheckBox ScbIsTaskbar;
        private CCWin.SkinControl.SkinLine skinLine1;
        private CCWin.SkinControl.SkinPanel skinPanel1;
        private CCWin.SkinControl.SkinComboBox ScbCount;
        private System.Windows.Forms.Label label1;
    }
}