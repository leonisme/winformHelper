﻿
namespace WToo
{
    partial class FrmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panFill = new System.Windows.Forms.Panel();
            this.labExit = new System.Windows.Forms.Label();
            this.labWinCpu = new System.Windows.Forms.Label();
            this.labTheColor = new System.Windows.Forms.Label();
            this.labClipboard = new System.Windows.Forms.Label();
            this.panTop = new System.Windows.Forms.Panel();
            this.labTitle = new System.Windows.Forms.Label();
            this.lab_network = new System.Windows.Forms.Label();
            this.panFill.SuspendLayout();
            this.panTop.SuspendLayout();
            this.SuspendLayout();
            // 
            // panFill
            // 
            this.panFill.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(243)))), ((int)(((byte)(249)))), ((int)(((byte)(241)))));
            this.panFill.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panFill.Controls.Add(this.lab_network);
            this.panFill.Controls.Add(this.labExit);
            this.panFill.Controls.Add(this.labWinCpu);
            this.panFill.Controls.Add(this.labTheColor);
            this.panFill.Controls.Add(this.labClipboard);
            this.panFill.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panFill.Location = new System.Drawing.Point(0, 45);
            this.panFill.Name = "panFill";
            this.panFill.Size = new System.Drawing.Size(1000, 555);
            this.panFill.TabIndex = 2;
            // 
            // labExit
            // 
            this.labExit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.labExit.BackColor = System.Drawing.Color.Transparent;
            this.labExit.Image = global::WToo.Properties.Resources.exitOut;
            this.labExit.Location = new System.Drawing.Point(949, 506);
            this.labExit.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labExit.Name = "labExit";
            this.labExit.Size = new System.Drawing.Size(40, 40);
            this.labExit.TabIndex = 0;
            this.labExit.Click += new System.EventHandler(this.labExit_Click);
            this.labExit.MouseEnter += new System.EventHandler(this.labExit_MouseEnter);
            this.labExit.MouseLeave += new System.EventHandler(this.labExit_MouseLeave);
            // 
            // labWinCpu
            // 
            this.labWinCpu.BackColor = System.Drawing.Color.Transparent;
            this.labWinCpu.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.labWinCpu.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(22)))), ((int)(((byte)(24)))), ((int)(((byte)(35)))));
            this.labWinCpu.Image = global::WToo.Properties.Resources.btnEnter;
            this.labWinCpu.Location = new System.Drawing.Point(285, 8);
            this.labWinCpu.Name = "labWinCpu";
            this.labWinCpu.Size = new System.Drawing.Size(124, 50);
            this.labWinCpu.TabIndex = 2;
            this.labWinCpu.Text = "CPU";
            this.labWinCpu.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.labWinCpu.Click += new System.EventHandler(this.labWinCpu_Click);
            this.labWinCpu.MouseEnter += new System.EventHandler(this.labControls_MouseEnter);
            this.labWinCpu.MouseLeave += new System.EventHandler(this.labControls_MouseLeave);
            // 
            // labTheColor
            // 
            this.labTheColor.BackColor = System.Drawing.Color.Transparent;
            this.labTheColor.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.labTheColor.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(22)))), ((int)(((byte)(24)))), ((int)(((byte)(35)))));
            this.labTheColor.Image = global::WToo.Properties.Resources.btnEnter;
            this.labTheColor.Location = new System.Drawing.Point(147, 8);
            this.labTheColor.Name = "labTheColor";
            this.labTheColor.Size = new System.Drawing.Size(124, 50);
            this.labTheColor.TabIndex = 1;
            this.labTheColor.Text = "拾色器";
            this.labTheColor.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.labTheColor.Click += new System.EventHandler(this.labTheColor_Click);
            this.labTheColor.MouseEnter += new System.EventHandler(this.labControls_MouseEnter);
            this.labTheColor.MouseLeave += new System.EventHandler(this.labControls_MouseLeave);
            // 
            // labClipboard
            // 
            this.labClipboard.BackColor = System.Drawing.Color.Transparent;
            this.labClipboard.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.labClipboard.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(22)))), ((int)(((byte)(24)))), ((int)(((byte)(35)))));
            this.labClipboard.Image = global::WToo.Properties.Resources.btnEnter;
            this.labClipboard.Location = new System.Drawing.Point(9, 8);
            this.labClipboard.Name = "labClipboard";
            this.labClipboard.Size = new System.Drawing.Size(124, 50);
            this.labClipboard.TabIndex = 0;
            this.labClipboard.Text = "剪切板";
            this.labClipboard.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.labClipboard.Click += new System.EventHandler(this.labClipboard_Click);
            this.labClipboard.MouseEnter += new System.EventHandler(this.labControls_MouseEnter);
            this.labClipboard.MouseLeave += new System.EventHandler(this.labControls_MouseLeave);
            // 
            // panTop
            // 
            this.panTop.BackColor = System.Drawing.Color.Transparent;
            this.panTop.BackgroundImage = global::WToo.Properties.Resources.Top;
            this.panTop.Controls.Add(this.labTitle);
            this.panTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.panTop.Location = new System.Drawing.Point(0, 0);
            this.panTop.Margin = new System.Windows.Forms.Padding(2);
            this.panTop.Name = "panTop";
            this.panTop.Size = new System.Drawing.Size(1000, 45);
            this.panTop.TabIndex = 1;
            this.panTop.MouseDown += new System.Windows.Forms.MouseEventHandler(this.panTop_MouseDown);
            // 
            // labTitle
            // 
            this.labTitle.AutoSize = true;
            this.labTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.labTitle.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(22)))), ((int)(((byte)(24)))), ((int)(((byte)(35)))));
            this.labTitle.Location = new System.Drawing.Point(227, 3);
            this.labTitle.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labTitle.Name = "labTitle";
            this.labTitle.Size = new System.Drawing.Size(95, 31);
            this.labTitle.TabIndex = 1;
            this.labTitle.Text = "工具集";
            // 
            // lab_network
            // 
            this.lab_network.BackColor = System.Drawing.Color.Transparent;
            this.lab_network.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lab_network.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(22)))), ((int)(((byte)(24)))), ((int)(((byte)(35)))));
            this.lab_network.Image = global::WToo.Properties.Resources.btnEnter;
            this.lab_network.Location = new System.Drawing.Point(427, 8);
            this.lab_network.Name = "lab_network";
            this.lab_network.Size = new System.Drawing.Size(124, 50);
            this.lab_network.TabIndex = 3;
            this.lab_network.Text = "网络调试";
            this.lab_network.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lab_network.Click += new System.EventHandler(this.lab_network_Click);
            // 
            // FrmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(204)))), ((int)(((byte)(204)))));
            this.ClientSize = new System.Drawing.Size(1000, 600);
            this.Controls.Add(this.panFill);
            this.Controls.Add(this.panTop);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "FrmMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "FrmMain";
            this.Load += new System.EventHandler(this.FrmMain_Load);
            this.panFill.ResumeLayout(false);
            this.panTop.ResumeLayout(false);
            this.panTop.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label labExit;
        private System.Windows.Forms.Panel panTop;
        private System.Windows.Forms.Label labTitle;
        private System.Windows.Forms.Panel panFill;
        private System.Windows.Forms.Label labClipboard;
        private System.Windows.Forms.Label labTheColor;
        private System.Windows.Forms.Label labWinCpu;
        private System.Windows.Forms.Label lab_network;
    }
}