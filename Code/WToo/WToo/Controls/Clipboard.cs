﻿using CCWin;
using CCWin.SkinControl;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WTool.IServices;

namespace WToo.Controls
{
    /// <summary>
    /// 粘贴板控制类
    /// </summary>
    public class Clipboard
    {
        IClipboard clipboard = new WTool.Services.Clipboard();//粘贴板服务
        /// <summary>
        /// 初始化面板
        /// </summary>
        public void CreateCent(Panel skinPanel, string count)
        {
            skinPanel.Controls.Clear();
            DataTable dt = clipboard.CreateCent(count);
            Point point = new Point(20, 20);
            Size size = new Size(140, 60);
            bool left = true;
            foreach (DataRow dr in dt.Rows)
            {
                RichTextBox tbox = new RichTextBox();
                tbox.Rtf = dr[1].ToString();
                tbox.Size = size;
                tbox.Location = point;
                tbox.SendToBack();
                tbox.BorderStyle = BorderStyle.None;
                tbox.ReadOnly = true;
                tbox.Click += new EventHandler(Rich_Click);
                tbox.BackColor = Color.FloralWhite;
                skinPanel.Controls.Add(tbox);
                if (left)
                {
                    point.X += 170;
                    left = false;
                }
                else
                {
                    point.Y += 70;
                    point.X = 20;
                    left = true;
                }
            }
        }

        /// <summary>
        /// 定义动态添加RichTextBox控件Click单击事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void Rich_Click(object sender, EventArgs e)
        {
            RichTextBox rtb = (RichTextBox)sender;
            System.Windows.Forms.Clipboard.SetDataObject(rtb.Text);
        }

        /// <summary>
        /// 清空粘贴板
        /// </summary>
        public void ClearClipboard(Panel skinPanel)
        {
            System.Windows.Forms.Clipboard.Clear();
            skinPanel.Controls.Clear();
            int count = clipboard.ClearData();
            MessageBoxForm msg = new MessageBoxForm();
            MessageBoxArgs arg = new MessageBoxArgs();
            arg.Text = $"共删除记录{count}条";
            msg.ShowMessageBoxDialog(arg);
        }

        /// <summary>
        /// 取得粘贴板数据
        /// </summary>
        public void DisplayClipboardData(Panel skinPanel, string count)
        {
            try
            {
                IDataObject iData = new DataObject();
                iData = System.Windows.Forms.Clipboard.GetDataObject();

                if (iData.GetDataPresent(DataFormats.Rtf))
                {
                    if (!string.IsNullOrEmpty((string)iData.GetData(DataFormats.Rtf)))
                    {
                        clipboard.InsertClipboard((string)iData.GetData(DataFormats.Rtf));
                    }
                    CreateCent(skinPanel, count);
                }
                else if (iData.GetDataPresent(DataFormats.Text))
                {
                    if (!string.IsNullOrEmpty((string)iData.GetData(DataFormats.Rtf)))
                    {
                        clipboard.InsertClipboard((string)iData.GetData(DataFormats.Rtf));
                    }
                    CreateCent(skinPanel, count);
                }
                else
                {
                    CreateCent(skinPanel, count);
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(e.ToString());
            }
        }
    }
}
