﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WTool.Model
{
    public class Clipboard
    {
        string id;//编号
        string content;//内容
        string datatime;//添加时间

        public string Id { get => id; set => id = value; }
        public string Content { get => content; set => content = value; }
        public string Datatime { get => datatime; set => datatime = value; }
    }
}
