﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WTool.Model.Base
{
    /// <summary>
    /// 全局配置，整个系统共用一套，所以采用单例模式
    /// </summary>
    public class Configuration
    {
        #region 单例
        private Configuration() { }

        /// <summary>
        /// 唯一实例化方法
        /// </summary>
        private static readonly Lazy<Configuration> lazy = new Lazy<Configuration>(()=>new Configuration());

        /// <summary>
        /// 提供对外唯一获取方法
        /// </summary>
        public static Configuration GetConfiguration
        {
            get {
                return lazy.Value;
            }
        }

        
        #endregion

        #region 变量
        bool _isExit = true;//是否可以关闭

        #endregion

        #region 封装字段
        [Description("是否可以关闭窗体")]
        public bool IsExit { get => _isExit; set => _isExit = value; }
        #endregion
    }
}
