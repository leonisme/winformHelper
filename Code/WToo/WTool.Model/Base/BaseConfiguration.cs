﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WTool.Model.Base
{
    /// <summary>
    /// 使用.NET 4的 Lazy 类型实现的单例模式
    /// </summary>
    public sealed class BaseConfiguration//公开的类及sealed标识;sealed:阻止其他类继承此类
    {
        private BaseConfiguration(){ }//私有的构造函数，不允许直接实例化

        /* Lazy:延迟初始化
         * 属性：
         * IsValueCreated:获取一个值，该值表示是否为该 Lazy<T> 实例创建了值
         * value:获取当前 Lazy<T> 实例的延迟初始化值。
         * 
         * 描述：
         * 默认情况下，类的所有公共和受保护成员 Lazy<T> 都是线程安全的，可从多个线程并发使用。 
         * 使用类型的构造函数的参数时，可以根据需要删除和每个线程安全保证
         * 
         * 
         * 搭建一个私有静态只读的Lazy对象，表明承载的对象为BaseConfiguration 并在（）中使用拉姆达进行实例化
         * **/
        private static readonly Lazy<BaseConfiguration> lazy = new Lazy<BaseConfiguration>(()=>new BaseConfiguration());

        /// <summary>
        /// 对外提供的唯一获取实例的构造函数
        /// </summary>
        public static BaseConfiguration GetBaseConfiguration
        {
            get {
                return lazy.Value;
            }
        }


    }
}
